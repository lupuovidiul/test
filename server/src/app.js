//NPM Dependencies
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
// const debug = require('debug');
require('dotenv').config(); //loading env variables

//LOCAL Dependencies
const healthCheckRoutes = require('./routes/healthCheck');
const loginRoutes = require('./routes/login');
const registerRoutes = require('./routes/register');
const usersRoutes = require('./routes/users');

//INITIALIZATION
const app = express();
const CORS_OPTIONS = {
  origin: 'http://localhost:3001',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['Set-Cookie'],
  preflightContinue: false,
  optionsSuccessStatus: 200,
};

//MIDDLEWARES
app.use(bodyParser.json());
app.use(cors(CORS_OPTIONS));

//ROUTES
app.use('/healthCheck', healthCheckRoutes);
app.use('/login', loginRoutes);
app.use('/register', registerRoutes);
app.use('/users', usersRoutes);
//global error handling <-- next(error); -->
app.use((error, req, res, next) => {
  console.log('GLOBAL APPLICATION HANDLER');
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.customMessage;
  res.status(status).json({ message: message });
});

const port = process.env.PORT;
app.listen(port, function() {
  console.log(`Server started on port ${port}`);
});
