const jwt = require('jsonwebtoken');

const isInternal = (req, res, next) => {
  if (req.user.role === 'internal') return next();
  const error = new Error();
  error.customMessage = 'Not authorized!';
  error.statusCode = 403;
  throw error;
};

module.exports = isInternal;
