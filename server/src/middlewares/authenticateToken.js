const jwt = require('jsonwebtoken');

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1]; //if token exists extract token else undefined
  if (!token) {
    const error = new Error();
    error.customMessage = 'Not authorized!';
    error.statusCode = 403;
    throw error;
  }

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      err.statusCode = 403;
      err.customMessage = 'Not authorized!';
      throw err;
    }
    req.user = user;
    next();
  });
};

module.exports = authenticateToken;
