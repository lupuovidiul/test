//NPM Dependencies
const express = require('express');
const router = express.Router();

//LOCAL Dependencies
const knex = require('../database');
const authenticateToken = require('../middlewares/authenticateToken');
const isInternal = require('../middlewares/isInternal');

router.get('/external', authenticateToken, isInternal, (req, res, next) => {
  knex
    .select('*')
    .from('users')
    .where('role', 'external')
    .then((users) => {
      res.json(
        users.map((user) => {
          return { id: user.id, userName: user.userName };
        }),
      );
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.customMessage = 'Failed getting external users!';
      next(error);
    });
});

router.delete('/:id', authenticateToken, isInternal, (req, res, next) => {
  knex('users')
    .where('id', parseInt(req.params.id, 10))
    .del()
    .then(() => {
      res.status(200).json({ customMessage: 'User deleted!' });
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.customMessage = 'Failed to delete user!';
      next(error);
    });
});

module.exports = router;
