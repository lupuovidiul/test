//NPM Dependencies
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');

//LOCAL Dependencies
const knex = require('../database');
const authenticateToken = require('../middlewares/authenticateToken');
const isInternal = require('../middlewares/isInternal');

const hashPasswordPromise = (password) => {
  return new Promise((resolve, reject) => {
    bcrypt
      .hash(password, 10)
      .then((hash) => resolve(hash))
      .catch((err) => reject(err));
  });
};

const register = (req, res, next, userRole) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errorMessage: errors.array()[0].msg });
  }

  hashPasswordPromise(req.body.password)
    .then((hashedPassword) => {
      knex
        .insert({
          userName: req.body.userName,
          password: hashedPassword,
          role: userRole,
        })
        .into('users')
        .returning(['id', 'userName'])
        .then((insertedUsers) => {
          console.log(insertedUsers);
          res.json({
            customMEssage: 'Registration completed!',
            user: insertedUsers[0],
          });
        });
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.customMessage = 'Registration failed!';
      next(error);
    });
};

const registerInternal = (req, res, next) => {
  register(req, res, next, 'internal');
};

const registerExternal = (req, res, next) => {
  register(req, res, next, 'external');
};

const validationMiddlewares = [
  body('userName')
    .isEmail()
    .withMessage('Insert a valid email!')
    .custom((value) => {
      return knex
        .select('*')
        .from('users')
        .where('userName', value)
        .first()
        .then((user) => {
          if (user) {
            return Promise.reject('User already exists!');
          }
        });
    }),
  body(
    'password',
    'Password should be alpha-numeric and 5 to 40 characters long!',
  )
    .isAlphanumeric()
    .isLength({
      min: 5,
      max: 40,
    }),
  body('confirmPassword').custom((value, { req }) => {
    if (value !== req.body.password) {
      throw new Error('Passwords have to match!');
    }
    return true;
  }),
];

router.post('/internal', validationMiddlewares, registerInternal);

router.post(
  '/external',
  authenticateToken,
  isInternal,
  validationMiddlewares,
  registerExternal,
);

module.exports = router;
