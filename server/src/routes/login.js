//NPM Dependencies
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

//LOCAL Dependencies
const knex = require('../database');

require('dotenv').config();

const loginCallbackFunction = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errorMessage: errors.array()[0].msg });
  }

  const userName = req.body.userName;
  const password = req.body.password;

  if (!userName || !password) {
    let err = new Error();
    err.customMessage = 'Login failed!';
    err.statusCode = 401;
    throw err;
  }

  let loadedUser;

  knex
    .select('*')
    .from('users')
    .where('userName', userName)
    .first()
    .then((dbUser) => {
      if (!dbUser) {
        let err = new Error();
        err.customMessage = 'Login failed!';
        err.statusCode = 401;
        throw err;
      }
      loadedUser = dbUser;
      return bcrypt.compare(password, dbUser.password);
    })
    .then((passwordsMatched) => {
      if (!passwordsMatched) {
        let err = new Error();
        err.customMessage = 'Login failed!';
        err.statusCode = 401;
        throw err;
      }

      const accessToken = jwt.sign(
        {
          id: loadedUser.id,
          userName: loadedUser.userName,
          role: loadedUser.role,
        },
        process.env.ACCESS_TOKEN_SECRET,
      );

      res.status(200).json({ accessToken: accessToken });
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      error.customMessage = 'Login failed!';
      next(error);
    });
};

router.post(
  '/',
  body('userName')
    .isEmail()
    .withMessage('Insert a valid email!'),
  loginCallbackFunction,
);

module.exports = router;
