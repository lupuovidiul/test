const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  const healthCheck = {
    status: 'UP',
  };
  try {
    res.status(200).json(healthCheck);
  } catch (error) {
    healthCheck.status = 'DOWN';
    healthCheck.message = 'Could not connect!';
    res.status(503).json(healthCheck);
  }
});

module.exports = router;
