//NPM Depenedencies
import React from 'react';
import PropTypes from 'prop-types';

const ExternalUsersList = ({ users, onDeleteClick }) => {
  return (
    <>
      <table className='table'>
        <thead>
          <tr>
            <th />
            <th>User Name</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {users.map((user) => {
            return (
              <tr key={user.id}>
                <td></td>
                <td>{user.userName}</td>
                <td>
                  <button
                    className='btn btn-outline-danger'
                    onClick={() => onDeleteClick(user.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

ExternalUsersList.propTypes = {
  users: PropTypes.array.isRequired,
};

export default ExternalUsersList;
