//NPM Dependencies
import React from 'react';
import PropTypes from 'prop-types';

//LOCAL Dependencies
import TextInput from '../../common/textInput';
let styles = {
  width: '50%',
  margin: 'auto',
};

const Register = ({ onChange, onSubmit, validations }) => {
  return (
    <div className='jumbotron' style={styles}>
      <form onSubmit={onSubmit}>
        {validations.map((element) => {
          return <p style={{ color: 'red' }}>{element}</p>;
        })}
        <TextInput
          id='userName'
          label='Email'
          name='userName'
          type='email'
          onChange={onChange}
        />
        <TextInput
          id='password'
          label='Password'
          name='password'
          type='password'
          onChange={onChange}
        />
        <TextInput
          id='confirmPassword'
          label='Confirm Password'
          name='confirmPassword'
          type='password'
          onChange={onChange}
        />
        <input type='submit' value='Register' className='btn btn-primary' />
      </form>
    </div>
  );
};

Register.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  validations: PropTypes.array.isRequired,
};

export default Register;
