//NPM Dependencies
import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  const activeStyle = { color: '#F15B2A' };
  return (
    <nav>
      <NavLink to='/login' activeStyle={activeStyle}>
        Login
      </NavLink>
      {' | '}
      <NavLink to='/register' activeStyle={activeStyle}>
        Register
      </NavLink>
    </nav>
  );
};

export default Header;
