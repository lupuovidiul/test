//NPM Dependencies
import React from 'react';
import PropTypes from 'prop-types';

function TextInput({ id, label, name, type, onChange }) {
  return (
    <div className='form-group'>
      <label htmlFor={id}>{label}</label>
      <div className='field'>
        <input
          id={id}
          type={type ? type : 'text'}
          name={name}
          className='form-control'
          onChange={onChange}
        />
      </div>
    </div>
  );
}

TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default TextInput;
