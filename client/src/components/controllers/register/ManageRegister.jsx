//NPM Dependencies
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

//LOCAL Dependencies
import Register from '../../views/register';
import * as userApi from '../../../api/userApi';

const ManageRegister = () => {
  const [user, setUser] = useState({});
  const [validations, setValidations] = useState([]);
  const [registrationHappend, setRegistrationHappend] = useState(false);

  function handleChange(event) {
    const { name, value } = event.target;
    setUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    userApi
      .registerInternal(user)
      .then((success) => {
        if (success) {
          toast.success('Registration completed!');
          setRegistrationHappend(true);
        } else setValidations(['Registration failed!']);
      })
      .catch((error) => {
        setValidations([error.customMessage]);
      });
  }

  function formIsValid() {
    const { userName, password, confirmPassword } = user;
    const errors = [];

    if (!userName) errors.push('Fill email field!');
    if (!password) errors.push('Fill password field!');
    if (!confirmPassword) errors.push('Fill confirm password field!');

    setValidations(errors);
    return errors.length === 0;
  }

  const registerForm = (
    <Register
      onChange={handleChange}
      onSubmit={handleSubmit}
      validations={validations}
    />
  );
  return registrationHappend ? <Redirect to={'/login'} /> : registerForm;
};

export default ManageRegister;
