//NPM Dependencies
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

//LOCAL Dependencies
import Login from '../../views/login';
import * as userApi from '../../../api/userApi';
import * as userActions from '../../../redux/actions/userActions';

const ManageLogin = ({ loggedUser, dispatch }) => {
  const [user, setUser] = useState({});
  const [validations, setValidations] = useState([]);

  function handleChange(event) {
    const { name, value } = event.target;
    setUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    userApi
      .logIn(user)
      .then((userFromJwt) => {
        dispatch(userActions.logInSuccess(userFromJwt));
      })
      .catch((error) => {
        setValidations([error.customMessage]);
      });
  }

  function formIsValid() {
    const { userName, password } = user;
    const errors = [];

    if (!userName) errors.push('Fill email!');
    if (!password) errors.push('Fill password!');

    setValidations(errors);
    return errors.length === 0;
  }

  const logInForm = (
    <Login
      onChange={handleChange}
      onSubmit={handleSubmit}
      validations={validations}
    />
  );

  let redirect = logInForm;
  if (loggedUser) {
    if (loggedUser.role === 'internal') {
      redirect = <Redirect to={'/externalUsers'} />;
    } else if (loggedUser.role === 'external') {
      redirect = <Redirect to={'/about'} />;
    }
  }
  return redirect;
};

function mapStateToProps(state, ownProps) {
  return { loggedUser: state.user.userName ? state.user : null };
}

export default connect(mapStateToProps)(ManageLogin);
