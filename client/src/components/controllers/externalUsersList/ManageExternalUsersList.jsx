//NPM Dependencies
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

//LOCAL Dependencies
import ExternalUsersList from '../../views/externalUsersList';
import * as userActions from '../../../redux/actions/userActions';
import * as userApi from '../../../api/userApi';
import Register from '../../views/register';

const ManageExternalUsersList = ({ externalUsers, dispatch }) => {
  const [user, setUser] = useState({});
  const [validations, setValidations] = useState([]);

  useEffect(() => {
    if (!externalUsers.length) {
      userApi
        .getExternalUsers()
        .then((externalUsers) => {
          dispatch(userActions.loadExternalUsersSuccess(externalUsers));
        })
        .catch((error) => {
          dispatch(userActions.loadExternalUsersSuccess([]));
        });
    }
  }, []);

  function handleDelete(userId) {
    userApi
      .deleteExternalUser(userId)
      .then((res) => {
        if (res) {
          toast.success('User deleted!');
          dispatch(userActions.deleteExternalUser(userId));
        } else toast.error('Failed deleting user!');
      })
      .catch((error) => {
        toast.error(error.customMessage + ' ' + error.message, {
          autoClose: false,
        });
      });
  }

  function handleChange(event) {
    const { name, value } = event.target;
    setUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    userApi
      .registerExternal(user)
      .then((insertExternalUser) => {
        if (insertExternalUser) {
          toast.success('Registration completed!');
          setUser({});
          dispatch(userActions.insertExternalUser(insertExternalUser));
        } else setValidations(['Registration failed!']);
      })
      .catch((error) => {
        setValidations([error.customMessage]);
      });
  }

  function formIsValid() {
    const { userName, password, confirmPassword } = user;
    const errors = [];

    if (!userName) errors.push('Fill email field!');
    if (!password) errors.push('Fill password field!');
    if (!confirmPassword) errors.push('Fill confirm password field!');

    setValidations(errors);
    return errors.length === 0;
  }

  return (
    <>
      <Register
        onChange={handleChange}
        onSubmit={handleSubmit}
        validations={validations}
      />
      <ExternalUsersList users={externalUsers} onDeleteClick={handleDelete} />
    </>
  );
};

ManageExternalUsersList.propTypes = {
  externalUsers: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  return { externalUsers: state.externalUsers };
}

export default connect(mapStateToProps)(ManageExternalUsersList);
