//NPM Dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './redux/configureStore';
import { BrowserRouter } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';

//LOCAL Dependencies
import App from './app';
import axios from './api/axios';

const store = configureStore();
const serverDownJsx = <h1> Server down </h1>;
const appJsx = (
  <ReduxProvider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ReduxProvider>
);

axios
  .get('/healthCheck')
  .then((response) => {
    if (response.status === 200) return appJsx;
    else return serverDownJsx;
  })
  .then((response) => {
    ReactDOM.render(response, document.getElementById('root'));
  })
  .catch((error) =>
    ReactDOM.render(serverDownJsx, document.getElementById('root')),
  );

// ReactDOM.render(<App />, document.getElementById('root'));
