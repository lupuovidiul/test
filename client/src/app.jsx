//NPM Dependencies
import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//LOCAL Dependencies
import ManageLogin from './components/controllers/login';
import ManageRegister from './components/controllers/register';
import Header from './components/common/header';
import ManageExternalUsersList from './components/controllers/externalUsersList';
import About from './components/common/about';

function App({ loggedUser }) {
  let routes = (
    <>
      <Header />
      <Switch>
        <Route path='/login' component={ManageLogin} />
        <Route path='/register' component={ManageRegister} />
        <Redirect to='/login' />
      </Switch>
    </>
  );

  if (loggedUser) {
    if (loggedUser.role === 'internal') {
      routes = (
        <Switch>
          <Route path='/login' component={ManageLogin} />
          <Route path='/externalUsers' component={ManageExternalUsersList} />
          <Route path='/about' component={About} />
        </Switch>
      );
    } else if (loggedUser.role === 'external') {
      routes = (
        <Switch>
          <Route path='/login' component={ManageLogin} />
          <Route path='/about' component={About} />
        </Switch>
      );
    }
  }

  // routes = (
  //   <>
  //     <Header />
  //     <Switch>
  //       <Route path='/login' component={ManageLogin} />
  //       <Route path='/register' component={ManageRegister} />
  //       <Route path='/externalUsers' component={ManageExternalUsersList} />
  //       <Route path='/about' component={About} />
  //       <Redirect to='/login' />
  //     </Switch>
  //   </>
  // );

  return (
    <div className='container-fluid'>
      {routes}
      <ToastContainer autoClose={3000} hideProgressBar />
    </div>
  );
}

App.propTypes = {
  loggedUser: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return { loggedUser: state.user.userName ? state.user : null };
}

export default connect(mapStateToProps, null)(App);
