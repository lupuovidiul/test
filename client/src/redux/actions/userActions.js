//LOCAL Dependencies
import * as actionTypes from './actionTypes';
//import * as userApi from '../../api/userApi';

export function logInSuccess(user) {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    user,
  };
}

export function loadExternalUsersSuccess(externalUsers) {
  return {
    type: actionTypes.LOAD_EXTERNAL_USERS_SUCCESS,
    externalUsers,
  };
}

export function deleteExternalUser(externalUserId) {
  return {
    type: actionTypes.DELETE_EXTERNAL_USER,
    externalUserId,
  };
}

export function insertExternalUser(externalUser) {
  return {
    type: actionTypes.INSERT_EXTERNAL_USER,
    externalUser,
  };
}
