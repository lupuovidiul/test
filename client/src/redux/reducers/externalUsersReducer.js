//LOCAL Dependencies
import * as actionTypes from '../actions/actionTypes';

export default function externalUsersReducer(state = [], action) {
  switch (action.type) {
    case actionTypes.LOAD_EXTERNAL_USERS_SUCCESS:
      return action.externalUsers;
    case actionTypes.DELETE_EXTERNAL_USER:
      return state.filter((user) => user.id !== action.externalUserId);
    case actionTypes.INSERT_EXTERNAL_USER:
      return [...state, action.externalUser];
    default:
      return state;
  }
}
