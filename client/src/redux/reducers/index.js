//NPM Depenedencies
import { combineReducers } from 'redux';

//LOCAL Dependencies
import user from './userReducer';
import externalUsers from './externalUsersReducer';

const rootReducer = combineReducers({
  user: user,
  externalUsers: externalUsers,
});

export default rootReducer;
