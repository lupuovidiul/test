//LOCAL Dependencies
import * as actionTypes from '../actions/actionTypes';

//export default function userReducer(state = initialState.user, action) {
export default function userReducer(state = {}, action) {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      return action.user;
    default:
      return state;
  }
}
