//NPM Dependencies
import { createStore, applyMiddleware, compose } from 'redux';
import reduxImutableStateInvariant from 'redux-immutable-state-invariant';

//LOCAL Dependencies
import rootReducer from './reducers';

export default function configureStore(initialState) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //add support for Redux dev tools
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(reduxImutableStateInvariant())), //warn us if  any any state in Redux store is mutated
  );
}
