//NPM Dependencies
import jwt from 'jsonwebtoken';
import setAuthorizationToken from './apiUtils';

//LOCAL Dependencies
import axios from './axios';

export function logIn(user) {
  return axios
    .post('/login', user)
    .then((response) => {
      if (response.status === 200) {
        const token = response.data.accessToken;
        localStorage.setItem('token', token);
        setAuthorizationToken(token);
        return jwt.decode(token);
      }
      return false;
    })
    .catch((error) => {
      let err = new Error(error);
      err.customMessage = error.customMessage
        ? error.customMessage
        : 'Login failed!';
      throw err;
    });
}

export function registerInternal(user) {
  return axios
    .post('/register/internal', user)
    .then((response) => {
      if (response.status === 200) {
        return true;
      }
      return false;
    })
    .catch((error) => {
      let err = new Error(error);
      err.customMessage = error.customMessage
        ? error.customMessage
        : 'Regsitration failed!';
      throw err;
    });
}

export function registerExternal(user) {
  return axios
    .post('/register/external', user)
    .then((response) => {
      if (response.status === 200) {
        return response.data.user;
      }
      return false;
    })
    .catch((error) => {
      let err = new Error(error);
      err.customMessage = error.customMessage
        ? error.customMessage
        : 'Regsitration failed!';
      throw err;
    });
}

export function getExternalUsers() {
  return axios
    .get('/users/external')
    .then((response) => {
      if (response.status === 200) {
        return response.data;
      }
      return false;
    })
    .catch((error) => {
      let err = new Error(error);
      err.customMessage = error.customMessage
        ? error.customMessage
        : 'Faild getting users!';
      throw err;
    });
}

export function deleteExternalUser(externalUserId) {
  return axios
    .delete('/users/' + externalUserId)
    .then((response) => {
      if (response.status === 200) {
        return true;
      }
      return false;
    })
    .catch((error) => {
      let err = new Error(error);
      err.customMessage = error.customMessage
        ? error.customMessage
        : 'Failed deleting user!';
      throw err;
    });
}
